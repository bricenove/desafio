# Teste desenvolvedor frontend Webjump - Miguel Briceño

Conhecimentos e habilidades frontend (HTML, CSS e JavaScript).

## Documentação

##Tecnologias usadas

Javascript, Gulp, CSS animado, FontAwesome, Sass.

Concentrei-me em duas premissas na busca pela solução do desafio, desempenho e performance.

##Performance

Reserve um tempo para detalhar a folha de estilo, com base no Skeleton CSS para criar a grid do site, procurando que o desempenho seja simples, amigável e de acordo com a função do site. Eu usei o Animate CSS, que é uma biblioteca que funciona com o WOW.Js, para animar o site, mas mantendo o foco na simplicidade. Eu fiz as folhas de estilo com o Sass, aplicando algumas das funções, como o uso de variáveis ​​e o aninhamento de estilos, dessa forma eu poderia acelerar um pouco o trabalho ao fazer alterações.

##Desempenho

Usei o Gulp pela primeira vez, pois não tinha experiência no uso dessa ferramenta, mas apesar disso e em busca de novos conhecimentos, pude entender rapidamente como era a operação básica. Eu apliquei para converter meus arquivos SCSS para CSS, e recebi um plugin de gulp que me permitiu combinar os estilos usados ​​com as últimas 3 versões do navegador.

Para resolver o desafio do layout use JavaScript clássico já que considerei que era a solução que poderia me dar maior performance sem afetar a velocidade de carregamento. Fiz os pedidos das APIs através do método Fetch, isso me permitiu gerar correções dos arquivos JSON, mostrar as categorias no menu principal e nos filtros. E do ID atribuído a cada categoria, fiz uma função que pesquisou a API para os produtos a serem exibidos.

## Iniciar o desenvolvimento
- Fork este repositório na sua conta do Bitbucket
- Instale as dependências
```
npm install
```
- Rode a aplicação
```
npm start
```
- Acesse http://localhost:8888
