const gulp = require('gulp');
const sass = require('gulp-sass');
const autoprefixer = require('gulp-autoprefixer');

gulp.task('sass', ()=>{
  gulp.src('public/scss/styles.scss')
      .pipe(sass({
          outputStyle:'compact'
      }))
      .pipe(autoprefixer({
          browsers:['last 3 versions']
      }))
      .pipe(gulp.dest('public/css'))
})

gulp.task('default', ()=>{
    gulp.watch('public/scss/styles.scss', ['sass'])
})